#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
struct postulaciones //iniciamos un nuevo esquema de registro con los atributos necesarios
{
	char facultad[1024];
	char carrera[1024];
	int codigo;
        int nem, rank, leng, mat, hist, cs, pond, psu, psucupos, beacupos;
	float max, min; //max y min estan en float ya que los valores son en formato "xx.x"
};

void poblar(struct postulaciones *admi)//pasamos la variable referenciada que está asociada a la estructura que definimos anteriormente
{
	FILE *Arq;//abrimos el archivo con los datos
	int i=0;//iniciamos un escalar
	if((Arq = fopen("Admicionuv2018.csv","r"))==NULL){ //si el archivo no se abre o no existe arroja error
		printf("Error al abrir el archivo");
	}
	else{ //de lo contrario sigue con el poblado de nuestra estructura
		while(feof(Arq)==0){//mientras no encuentre el final del archivo seguira iterando
			//usamos fscanf ya que nos conviene segun el formato que le dimos al txt que pasamos a csv y espaciamos y pusimos el atributo facultad a cada linea
			fscanf(Arq,"%s %s %d %d %d %d %d %d %d %d %d %f %f %d %d",admi[i].facultad,admi[i].carrera,&admi[i].codigo,&admi[i].nem,&admi[i].rank,&admi[i].leng,&admi[i].mat,&admi[i].hist,&admi[i].cs,&admi[i].pond,&admi[i].psu,&admi[i].max,&admi[i].min,&admi[i].psucupos,&admi[i].beacupos);		
			/* como nuestro formato separa cada atributo con espacios (los nombres estan sin espacios) simplemente definimos el formato de fscanf con 15 datos y asignamos cada uno de ellos a su respectiva variable dentro de nuestra estructura  */
			i++;
		}
	}
	fclose(Arq);
}
void ConsultarPond(struct postulaciones admi[])//recive como parametro la estructura ya poblada
{
	char carre[1024]; //el usuario ingresa el nombre y lo almacena en la variable carre
	printf("Ingrese  el nombre el nombre de la carrera sin espacios: ");
	scanf("%s",&carre);
	int i,cont=0;	
	for(i=0;i<54;i++) //luego en un ciclo busca coincidencias por el nombre dentro de la variable .carrera que pertenece a nuestra estructura poblada
	{
		if(strcmp(admi[i].carrera,carre)==0)//si encuentra coincidencias entonces arroja los datos requeridos
		{
			printf("\n##PONDERACIONES##\n");
			printf("\nNem: %d",admi[i].nem);
			printf("\nRanking:%d",admi[i].rank);
			printf("\nLenguaje: %d",admi[i].leng);
			printf("\nMatematicas: %d",admi[i].mat);
			printf("\nHistoriaCS: %d",admi[i].hist);
			printf("\nCiencias: %d",admi[i].cs);
			printf("\nUltimo Seleccionado: %f \n",admi[i].min);
			cont++;
		}
	}
	if(cont==0)
	{
		printf("\nLo sentimos no hemos encontrado su carrera\n");
	} 			
}
void Simulacion(struct postulaciones admi[]){
	int nem,ranking,lenguaje,mate,historia,ciencias,puntaje; //variables para almacenar los porcentajes/ponderaciones de cada cosa
	char carre[1024]; 
	printf("\nPara simular tu puntaje ingrese sus datos\n");//se piden los datos respectivos para poder hacer la simulacion
	printf("NEM: ");
	scanf("%d",&nem);
	printf("\nRanking: ");
	scanf("%d",&ranking);
	printf("\nLenguaje: ");
	scanf("%d",&lenguaje);
	printf("\nMatematicas: ");
	scanf("%d",&mate);
	printf("\nHistoria: ");
	scanf("%d",&historia); 
	printf("\nCiencias: ");
	scanf("%d",&historia);
	printf("\nCarrera: ");
	scanf("%s",&carre);//la carrera para poder buscar nuevamente la coincidencia
	int i=0,cont=0;
	for(i=0;i<54;i++)
	{
		if(strcmp(admi[i].carrera,carre)==0)//misma busqueda que en consultarpond con la excepcion que ahora operaremos con los datos
		{
			printf("\n##PONDERACIONES##\n");
			printf("\nNem: %d",admi[i].nem);
			printf("\nRanking:%d",admi[i].rank);
			printf("\nLenguaje: %d",admi[i].leng);
			printf("\nMatematicas: %d",admi[i].mat);
			printf("\nHistoriaCS: %d",admi[i].hist);
			printf("\nCiencias: %d",admi[i].cs);
			printf("\nUltimo Seleccionado: %f \n",admi[i].min);
			cont++;
			puntaje=0;
			//sabemos que se puede tomar historia ó ciencias al momento de ponderar por lo que las condiciones a continuacion son para decidir cual tomar
			//si la ponderacion de historia es igual a 0 quiere decir que la de ciencias es obligatoria para la carrera y la operacion solo toma el ponderado de ciencias
			if(admi[i].hist==0){
				puntaje=(((nem*admi[i].nem)/100)+((ranking*admi[i].rank)/100)+((lenguaje*admi[i].leng)/100)+((mate*admi[i].mat)/100)+((ciencias*admi[i].cs)/100));
			}
			//lo mismo que arriba pero a la inversa 
			else if(admi[i].cs==0){
				puntaje=(((nem*admi[i].nem)/100)+((ranking*admi[i].rank)/100)+((lenguaje*admi[i].leng)/100)+((mate*admi[i].mat)/100)+((historia*admi[i].hist)/100));
			}
			//si ambas ponderaciones son distintas de 0 quiere decir que la mayor ponderacion entre ciencias y historia se suma a la operacion
			else if(admi[i].hist!=0 && admi[i].cs!=0){
				if(admi[i].hist>admi[i].cs){
					puntaje=(((nem*admi[i].nem)/100)+((ranking*admi[i].rank)/100)+((lenguaje*admi[i].leng)/100)+((mate*admi[i].mat)/100)+((historia*admi[i].hist)/100));
				}
				else{
					puntaje=(((nem*admi[i].nem)/100)+((ranking*admi[i].rank)/100)+((lenguaje*admi[i].leng)/100)+((mate*admi[i].mat)/100)+((ciencias*admi[i].cs)/100));
				}			
			}
			//como se requiere aqui vamos guardando todos los supuestos en el archivo Supuestos.txt con fprintf
			FILE *sup;
			sup=fopen("Supuestos.txt","wt");
			fprintf(sup,"\nCarrera: %s\n##PONDERACIONES##\nNem: %d\nRanking: %d\nLenguaje: %d\nMatematicas: %d\nHistoriaCS: %d\nCiencias: %d\nUltimo Seleccionado: %f \nSu Puntaje Simulado: %d",admi[i].carrera,admi[i].nem,admi[i].rank,admi[i].leng,admi[i].mat,admi[i].hist,admi[i].cs,admi[i].min,puntaje);
			fclose(sup);		
		}
	}
	/*de algo que nos dimos cuenta es que en la imagen de ejemplo en donde se simula el puntaje se suma historia y ciencias al mismo tiempo lo cual es erroneo ya que es uno o lo otro no los 2
	haciendolo como deberia ser da 280 ponderado con los datos que se mostraban ahi y no 310 que es lo que da si se suman ambas*/
	if(cont==0)
	{
		printf("\nLo sentimos no hemos encontrado su carrera\n");
	}
	printf("\n Su puntaje simulado: %d\n",puntaje);	
}

void MostrarPond(struct postulaciones *admi){//es basicamente la misma busqueda que las ponderaciones por carrera solo que esta vez es por facultad y se muestran todas las carreras
	char facu[1024];	
	printf("Ingrese el nombre de la Facultad sin espacios y en mayuscula: ");
	scanf("%s",&facu);
	int i=0,cont=0;
	for(i=0;i<54;i++)
	{
		if(strcmp(admi[i].facultad,facu)==0)//ahora se busca por coincidencia una facultad por lo que es posible aparesca mas de un resultado, a diferencia que anteriormente
		{
			printf("\n Carrera: %s \ncodigo: %d \nPONDERACIONES \nNEM: %d \nRanking: %d \nlenguaje: %d \nmatematicas: %d \nhistoria: %d \nciencias%d \nPuntaje minimo de postulacion ponderado: %d psu: %d \nPrimer y ultimo matriculado max: %f min: %f \nCUPOS psu:%d bea:%d\n",admi[i].carrera,admi[i].codigo,admi[i].nem,admi[i].rank,admi[i].leng,admi[i].mat,admi[i].hist,admi[i].cs,admi[i].pond,admi[i].psu,admi[i].max,admi[i].min,admi[i].psucupos,admi[i].beacupos);
			cont++;
		}	
	}
	if(cont==0)
	{
		printf("\nLo sentimos no hemos encontrado su Facultad\n");
	}
}

int main() {
	struct postulaciones admi[54];//definimos un arreglo de estructuras/registros para guardar cada linea del archivo de texto	
	poblar(admi);//llamamos a el proceso poblar con el arreglo como parametro
	int opcion;//un menu simple que valida las entradas para hacer el programa un poco mas amigable
	while (opcion!=4)
	{
			printf("1.- Consultar Ponderacion de Carrera\n");
			printf("2.- Simular Postulación de la Carrera\n");
			printf("3.- Mostrar ponderaciones de Facultad\n");
			printf("4.- Salir\n");
			scanf("%d",&opcion);
		
			if(opcion==1)
			{
				ConsultarPond(admi);
			}
			if(opcion==2)
			{
				Simulacion(admi);
			}
			if(opcion==3){
				MostrarPond(admi);			
			}				
			else if(opcion!=1 && opcion!=2 & opcion!=3 & opcion!=4){
				printf("\nOpcion Invalida porfavor ingresela nuevamente: \n");
			}
	}
}

